<?php

class Template extends Core {

    public function __construct($inject)
    {
        $authRequired = true;
        parent::__construct($inject,$authRequired);
        //$this->inject->output = "string";
        //$this->inject->cacheExpires = 5; //in seconds
    }

    public function __call($method, $args)
    {
        return $this->inject->lang->requestNotFound;
    }
}

?>
